from guardian import GuardState
import time

## FUNCTIONS

def adj_rh_power(power_adj):
    ezca['SETUPPERPOWER'] = power_adj
    ezca['SETLOWERPOWER'] = power_adj

## insert error checker


 ## STATES ##

nominal = 'NOMINAL'

class INIT(GuardState):
     def run(self):
        return 'NOMINAL'

class NOMINAL(GuardState):
    index = 1
    goto = True
    def main(self):
        ezca['INVERSE_FILTER_MASK'] = 1
        self.lower_power = ezca['SETLOWERPOWER']

    def run(self):
        if self.lower_power != ezca['SETLOWERPOWER']:
            ezca['INVERSE_INVERSE_FILTER_IN'] = ezca['SETLOWERPOWER']
            self.lower_power = ezca['SETLOWERPOWER']
        return True


class FILTER_RH_INPUT(GuardState):
    index = 2
    def main(self):
       self.timer['wait'] = 0.0
       ezca['INVERSE_FILTER_MASK'] = 0
       log('Adjusting the RH power')
       
    def run(self):
        self.round_val = 4
        if self.timer['wait'] :
            # Added a round to reduce the logging. I dont think we care if 2.000 or 2.0000000005 - TJ
            self.RECENT_FILTER_REQUEST = round(ezca['INVERSE_FILTER_OUTVAL'], self.round_val)
            if (self.RECENT_FILTER_REQUEST) < 0:
                log('RH attempting to go to negative power')
                self.RECENT_FILTER_REQUEST = 0.0

            # NOTE: In an effor to reduce logging, I added this check in - TJ
            # Check if the power is different, if it is set both it and the timer
            if round(ezca['SETUPPERPOWER'], self.round_val) != self.RECENT_FILTER_REQUEST:
                    ezca['SETUPPERPOWER'] = self.RECENT_FILTER_REQUEST
                    ezca['SETLOWERPOWER'] = self.RECENT_FILTER_REQUEST
                    self.timer['wait'] = 1.0
            #adj_rh_power(self.RECENT_FILTER_REQUEST)
            #log('{}'.format(ezca['INVERSE_FILTER_OUTVAL']))
            #self.timer['wait'] = 1.0
        return True


class RESET(GuardState):
    index = 3
    def main(self):
        self.state = ezca['INVERSE_FILTER_MASK']
        if self.state > 0 :
            # we are in nominal state
            self.val = ezca['SETUPPERPOWER']
            ezca['INVERSE_INVERSE_FILTER_IN'] = self.val
            ezca['PRIORVAL'] = self.val
            ezca['INVERSE_FILTER_IN'] = self.val
            time.sleep(0.1)
            ezca['INVERSE_INVERSE_FILTER_RSET'] = 2
            time.sleep(0.1)
            ezca['INVERSE_FILTER_RSET'] = 2
            return True

        else:
            # we are in filter RH  state
            self.val = ezca['INVERSE_FILTER_IN']
            ezca['PRIORVAL'] = self.val
            ezca['INVERSE_INVERSE_FILTER_IN'] = self.val
            ezca['INVERSE_FILTER_IN'] = self.val
            time.sleep(0.1)
            ezca['INVERSE_FILTER_RSET'] = 2
            ezca['INVERSE_INVERSE_FILTER_RSET'] = 2
            return True



edges = [('INIT', 'NOMINAL'),
         ('NOMINAL', 'INIT'),
         ('RESET', 'NOMINAL'),
         ('NOMINAL', 'RESET'),
         ('RESET', 'FILTER_RH_INPUT'),
         ('FILTER_RH_INPUT', 'RESET')]
